var rect = require ('./rectangle2')

function  solveRect(l,b) {
    console.log("Solving for a rectangle  with l= " + l + " b= " + b );
    rect(l,b, (err, rectangle) => {
        if (err){
            console.log(err);
        }
        else  {
            console.log("Area  for -l = "+l+ "- b = "+b +" is " + rectangle.area());
            console.log("Perimeter for -l = "+l+ " -b = "+ b +" is " + rectangle.perimeter());
        }
    });
    console.log("This statement is after call to the rect");
}

solveRect(4,5);
solveRect(3,5)
solveRect(0 ,5)
solveRect(-3 ,-3);
